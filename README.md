<kbd><strong>[former vscode user](https://neovim.io/)</strong></kbd>
&thinsp;<kbd><strong>[nightfall dimmed enjoyer](https://gitlab.com/djaruun/nightfall-dimmed.nvim)</strong></kbd>

# Hello there 👋🏼

##### *wannabe programmer using windows wishing it was linux*<br>
Peep the <strong>[projects tab](https://gitlab.com/users/djaruun/projects)</strong> if you wanna see my projects or visit <strong>[my website](https://djaruun.gitlab.io)</strong> for more of me!

---

#### Some things I enjoy
[![Gleam](https://gleam.run/images/lucy/lucy.svg){height=45}](https://gleam.run)
&nbsp;&nbsp;[![Elixir](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fseeklogo.com%2Fimages%2FE%2Felixir-logo-CF24E6FA55-seeklogo.com.png&f=1&nofb=1&ipt=4f06f8f06056ad6995ab6957416fae6981f606f1bafdb9fa2e3aba6276f00a33&ipo=images){height=41}](https://elixir-lang.org)
&nbsp;&nbsp;&nbsp;[![Svelte](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Svelte_Logo.svg/1200px-Svelte_Logo.svg.png){height=38}](https:/svelte.dev)
&nbsp;[![Go](https://miro.medium.com/v2/resize:fit:1000/0*YISbBYJg5hkJGcQd.png){height=38}](https://go.dev)

---

#### Cool things I've made
<kbd>&thinsp;![](https://gitlab.com/uploads/-/system/project/avatar/59006208/dotfiles.jpg?width=32){width=17} <strong>[My dotfiles](https://gitlab.com/djaruun/dotfiles)</strong>&nbsp;</kbd>
&thinsp;<kbd>&thinsp;![](https://gitlab.com/uploads/-/system/project/avatar/60270983/communityIcon_n2hvyn96zwk81.png?width=48){width=17} <strong>[clipboard.nvim](https://gitlab.com/djaruun/clipboard.nvim)</strong>&nbsp;</kbd>
&thinsp;<kbd>&thinsp;![](https://avatars.githubusercontent.com/u/89078636?s=32&v=4){width=17} <strong>[Chucktype.svelte](https://github.com/DJAruun/chucktype.svelte)</strong>&nbsp;</kbd>
&thinsp;<kbd>&thinsp;![](https://gitlab.com/uploads/-/system/project/avatar/57442101/appicon.png?width=48){width=17} <strong>[GOllama](https://gitlab.com/djaruun/gollama)</strong>&nbsp;</kbd>

---

#### My social links
<kbd>&thinsp;![](https://images.ctfassets.net/xz1dnu24egyd/3FbNmZRES38q2Sk2EcoT7a/a290dc207a67cf779fc7c2456b177e9f/press-kit-icon.svg){width=19} <strong>[@djaruun](https://gitlab.com/djaruun)</strong>&nbsp;</kbd>
&thinsp;<kbd>&thinsp;![](https://docs.modrinth.com/img/logo.svg){width=18} <strong>[DJARUUN](https://modrinth.com/user/DJARUUN)</strong>&nbsp;</kbd>
&thinsp;<kbd>&thinsp;![](https://assets-global.website-files.com/6257adef93867e50d84d30e2/636e0a6a49cf127bf92de1e2_icon_clyde_blurple_RGB.png){width=23} <strong>[@djaruun](https://discord.com)</strong>&nbsp;</kbd>
&thinsp;<kbd>&thinsp;![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Twitter_and_X_logos.svg/2560px-Twitter_and_X_logos.svg.png){width=39} <strong>[@djaruun1](https://x.com/djaruun1)</strong>&nbsp;</kbd>
&thinsp;<kbd>&thinsp;![](https://cdn-icons-png.flaticon.com/512/25/25231.png){width=16} <strong>[DJAruun](https://github.com/DJAruun)</strong>&nbsp;</kbd>